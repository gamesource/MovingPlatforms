# Moving platforms
## tags

for the beginning, you need two tags:
1) Player tag - for the `MoveTrigger.cs` at line 23
2) MovingPlatform - for the `StickToPlatform.cs` at lines 28 and 41

## setup

moving platform consists of 5 objects
1) Root object, where you can put Move script and MoveEditorHelper script. Editor helper is really useful!
2) Moving Pivot object, is a parent of the platform, should have scale of (1,1,1)
3) Start object, which is empty object to indicate where platform starts
4) End object, which is empty object to indicate where platform ends
5) Platform object, child if the MovingPivot object. Needs Rigidbody and Collider, may be any scale and have any rotation.

player object also needs to have `StickToPlatform` script on it. You may not set up values of the script, they are calculated dynamically

## configuration
### Move

TargetTransform - should be MovingPivot object
Start position - should be Start object
End position - should be End object
Cycle speed - how many cycles your platform do per one second
isActive - is your platform activated by default, or you need a trigger to activate it

MovingCurveX - how x channel of transform is changing per T
MovingCurveY - how y channel of transform is changing per T
MovingCurveZ - how z channel of transform is changing per T

### MoveEditorHelper

HideOnPlay - enable to hide all gizmos once play button is pressed
