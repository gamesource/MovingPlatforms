﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Move))]
[ExecuteInEditMode]
public class MoveEditorHelper : MonoBehaviour
{
	public bool HideOnPlay;
	
	private Move move;
	
	/// <summary>
	/// this variables are used to help you use editor mode, ignore them
	/// </summary>
	private bool isInEditorMode = true;
	private Transform actualPlatform;
	private Mesh actualMesh;

	private bool enabled;
	
	// Use this for initialization
	void Start ()
	{
		move = GetComponent<Move>();
		actualPlatform = move.TargetTransform.GetChild(0);
		actualMesh = actualPlatform.GetComponent<MeshFilter>().sharedMesh;
	}

	private void Update()
	{
		if (HideOnPlay)
		{
			if (UnityEditor.EditorApplication.isPlaying)
			{
				enabled = false;
			}
			else
			{
				enabled = true;
			}
		}

		if (!UnityEditor.EditorApplication.isPlaying)
		{
			move.SetLerpPosition(ref move.TargetTransform, move.StartTransform, move.EndTransform, move.T);
			actualPlatform.localPosition = Vector3.zero;
		}
	}

	private void OnEnable()
	{
		enabled = true;
		move = GetComponent<Move>();
		actualPlatform = move.TargetTransform.GetChild(0);
		actualMesh = actualPlatform.GetComponent<MeshFilter>().sharedMesh;
	}

	private void OnDisable()
	{
		enabled = false;
	}

	private void OnDrawGizmos()
	{
		if (enabled)
		{
			Gizmos.color = new Color(0.2f, 1f, 0f, 0.4f);
			Gizmos.DrawMesh(actualMesh, move.StartTransform.position, actualPlatform.transform.rotation,
				actualPlatform.transform.localScale * 1.025f);
			Gizmos.color = new Color(1f, 0.7f, 0f, 0.4f);
			Gizmos.DrawMesh(actualMesh, move.EndTransform.position, actualPlatform.transform.rotation,
				actualPlatform.transform.localScale * 1.025f);
		}
	}
}
