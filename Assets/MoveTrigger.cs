﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MoveTrigger : MonoBehaviour
{
	[Header("Will be discovered automatically")]
	[SerializeField]
	Move MoveParent;

	private void Start()
	{
		MoveParent = transform.parent.parent.GetComponent<Move>();
	}

	/// <summary>
	/// Just activate moving platform on collision
	/// </summary>
	/// <param name="other"></param>
	private void OnCollisionEnter(Collision other)
	{
		if (other.collider.CompareTag("Player"))
		{
			MoveParent.isActive = true;
		}
	}
}
