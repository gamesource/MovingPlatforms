﻿using System.Collections;
using System.Security.Cryptography;
using UnityEngine;

/// <summary>
/// Take a note, that we are animating not the platform, but it's root object. 
/// So you can set your platform any scale and rotation you want, 
/// !!! just keep it's root rotation to (0,0,0) and scale to (1,1,1) !!!
/// and have fun 
/// cheers, from INshadow
/// 
/// ░░░░░░░░░▄░░░░░░░░░░░░░░▄
/// ░░░░░░░░▌▒█░░░░░░░░░░░▄▀▒▌
/// ░░░░░░░░▌▒▒█░░░░░░░░▄▀▒▒▒▐
/// ░░░░░░░▐▄▀▒▒▀▀▀▀▄▄▄▀▒▒▒▒▒▐
/// ░░░░░▄▄▀▒░▒▒▒▒▒▒▒▒▒█▒▒▄█▒▐
/// ░░░▄▀▒▒▒░░░▒▒▒░░░▒▒▒▀██▀▒▌
/// ░░▐▒▒▒▄▄▒▒▒▒░░░▒▒▒▒▒▒▒▀▄▒▒▌
/// ░░▌░░▌█▀▒▒▒▒▒▄▀█▄▒▒▒▒▒▒▒█▒▐
/// ░▐░░░▒▒▒▒▒▒▒▒▌██▀▒▒░░░▒▒▒▀▄▌
/// ░▌░▒▄██▄▒▒▒▒▒▒▒▒▒░░░░░░▒▒▒▒▌
/// ▌▒▀▐▄█▄█▌▄░▀▒▒░░░░░░░░░░▒▒▒▐
/// ▐▒▒▐▀▐▀▒░▄▄▒▄▒▒▒▒▒▒░▒░▒░▒▒▒▒▌
/// ▐▒▒▒▀▀▄▄▒▒▒▄▒▒▒▒▒▒▒▒░▒░▒░▒▒▐
/// ░▌▒▒▒▒▒▒▀▀▀▒▒▒▒▒▒░▒░▒░▒░▒▒▒▌
/// ░▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒░▒░▒░▒▒▄▒▒▐
/// ░░▀▄▒▒▒▒▒▒▒▒▒▒▒░▒░▒░▒▄▒▒▒▒▌
/// ░░░░▀▄▒▒▒▒▒▒▒▒▒▒▄▄▄▀▒▒▒▒▄▀
/// ░░░░░░▀▄▄▄▄▄▄▀▀▀▒▒▒▒▒▄▄▀
/// ░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▀▀
/// 
///         woof!
/// </summary>
public class Move : MonoBehaviour
{
    [SerializeField]
    [Range(0f, 1f)]
    protected float _t = 0;

    public float T
    {
        get { return _t; }
    }
    
    [Header("Target transform, which we animate")] 
    public Transform TargetTransform;
    
    [Header("Start position")] 
    public Transform StartTransform;
    
    [Header("End position")] 
    public Transform EndTransform;
    

    [Header("Cycle speed. Default is 1 cycle per second")] 
    public float CycleSpeed;

    [Header("Are we active now? Say me when i am going to move, coz i'm lazy...")]
    public bool isActive;

    [Header("You can animate each coordinate at different rate")] 
    public AnimationCurve movingCurveX;
    public AnimationCurve movingCurveY;
    public AnimationCurve movingCurveZ;

    // those three variables below may seems to be useless, but they are protected, and you can access them from any child class and use
    
    /// <summary>
    /// calculated delta, may be not needed at all, but we still do it
    /// </summary>
    protected Vector3 delta;
    /// <summary>
    /// calculated direction... same shit
    /// </summary>
    protected Vector3 direction;
    /// <summary>
    /// this as well
    /// </summary>
    protected float magnitude;


    // this one is extremely useful for making your platform move and stop any time you want
    
    /// <summary>
    /// Coroutine in which we are moving out target
    /// </summary>
    protected Coroutine _movingCoroutine;

    
    /// <summary>
    /// this variable is used to calculate vector3 by channels without instantiating new vector3 instance each time
    /// </summary>
    protected Vector3 tempPos;
    

    public void Attach(Transform target)
    {
        target.parent = TargetTransform;
    }
    
    protected void Start()
    {
        delta = EndTransform.localPosition - StartTransform.localPosition;
        direction = delta.normalized;
        magnitude = delta.magnitude;
        TargetTransform.localPosition = StartTransform.localPosition;

        _movingCoroutine = StartCoroutine(MovingRoutine());
    }

    /// <summary>
    /// by calling this method you can reset animation from initial state
    /// </summary>
    public void ResetAnimation()
    {
        if (_movingCoroutine != null)
            StopCoroutine(_movingCoroutine);      
        _movingCoroutine = StartCoroutine(MovingRoutine());
    }


    /// <summary>
    /// Performs animation in cycles. Position is updated each frame, just like in Update function
    /// </summary>
    /// <returns></returns>
    protected IEnumerator MovingRoutine()
    {
        yield return false;
        

        // Do it again!
        while (true)
        {
            if (isActive)
            {
                while (_t < 1f)
                {
                    SetLerpPosition(ref TargetTransform, StartTransform, EndTransform, _t);
                    yield return new WaitForEndOfFrame();
                    _t += Time.deltaTime * CycleSpeed;
                }
                _t = 1f;
                SetLerpPosition(ref TargetTransform, StartTransform, EndTransform, _t);
                yield return new WaitForEndOfFrame();
                
                while (_t > 0f)
                {
                    SetLerpPosition(ref TargetTransform, StartTransform, EndTransform, _t);
                    yield return new WaitForEndOfFrame();
                    _t -= Time.deltaTime * CycleSpeed;
                }
                _t = 0f;
                SetLerpPosition(ref TargetTransform, StartTransform, EndTransform, _t);
                yield return new WaitForEndOfFrame();
            }
            else
            {
                yield return false;
            }
        }
    }

    /// <summary>
    /// Sets position between start and end transforms depending on `t` parameter and accordingly to the animation curves per each channel
    /// </summary>
    /// <param name="target">target transform we are settign local pos for</param>
    /// <param name="from">from where we start</param>
    /// <param name="to">where we are going</param>
    /// <param name="t">where we are, 0 to 1</param>
    public void SetLerpPosition(ref Transform target, Transform from, Transform to, float t)
    {
        tempPos.x = Mathf.Lerp(from.localPosition.x, to.localPosition.x,
            movingCurveX.Evaluate(t));
        tempPos.y = Mathf.Lerp(from.localPosition.y, to.localPosition.y,
            movingCurveY.Evaluate(t));
        tempPos.z = Mathf.Lerp(from.localPosition.z, to.localPosition.z,
            movingCurveZ.Evaluate(t));
        
        target.localPosition = tempPos;
    }
}