﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class StickToPlatform : MonoBehaviour
{
	[Header("this parent is original one, it will be restored after you leave the platform")]
	[SerializeField]
	protected Transform originalParent;
	[Header("this parent is current one, you are parented to platform root as soon as you touch it")]
	[SerializeField]
	protected  Transform currentParent;
	
	
	// Use this for initialization
	void Start ()
	{
		originalParent = transform.parent;
	}

	/// <summary>
	/// If collided with the platform, parent self to it's root
	/// </summary>
	/// <param name="other"></param>
	private void OnCollisionEnter(Collision other)
	{
		if (other.collider.CompareTag("MovingPlatform"))
		{
			transform.SetParent(other.collider.transform.parent,true);
			currentParent = transform.parent;
		}
	}

	/// <summary>
	/// If left the platform - restore original parent
	/// </summary>
	/// <param name="other"></param>
	private void OnCollisionExit(Collision other)
	{
		if (other.collider.CompareTag("MovingPlatform"))
		{
			if (transform.parent == other.collider.transform.parent) 
				transform.SetParent(originalParent,true);
			currentParent = transform.parent;
		}
	}
}
